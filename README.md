<h1 align="center">
  <br>
  <img align="right" src="assets/penguin.png" alt="Penguim logo" width="60" >
  <br>
  Papo de Madagascar
  <br>
</h1>

Bem-vindo ao repositório do nosso Grupo de Estudos DevOps! Este grupo é dedicado a explorar e aprender sobre DevOps e tecnologias relacionadas através de encontros regulares.

## 💬 Descrição
Nosso grupo se reúne regularmente para discutir e aprender sobre temas variados dentro do universo DevOps. Cada encontro é apresentado por um membro diferente, que traz um tema de sua escolha para compartilhar com o grupo.

## 🤔 Como sugerir um tema
Se você tem um tema interessante que gostaria de apresentar, por favor, abra uma issue utilizando nosso template de sugestão de tema. Para isso, basta [abrir uma nova issue para sugerir um tema](https://gitlab.com/devops-study-br/papo-de-madagascar/-/issues/new?issuable_template=sugestao_tema)

## 📅 Calendário de encontros

|   Data   |                Tema                 |   Apresentador   |
|:--------:|:-----------------------------------:|:----------------:|
| 01/06/24 | Kuberntes "RBAC"                    | Thadeu           |


---

**Observação:** Este README está em constante atualização. Fique de olho para novas informações e atualizações no calendário.

<a href="https://www.flaticon.com/free-icons/penguin" title="penguin icons">Penguin icons created by Mihimihi - Flaticon</a>

